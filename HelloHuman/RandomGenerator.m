//
//  RandomGenerator.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-08.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "RandomGenerator.h"

@implementation RandomGenerator
static NSMutableArray *zoneNames = nil;
static NSMutableSet *usedZoneNames = nil;

static NSMutableArray *zoneTypes = nil;

static NSMutableArray *species = nil;

static NSMutableArray *animalNames = nil;
static NSMutableSet *usedAnimalNames = nil;

static NSMutableArray *firstMeetingPhrases = nil;
static NSMutableArray *normalMeetingPhrases = nil;
static NSMutableArray *friendMeetingPhrases = nil;

+ (RandomGenerator *)sharedRandom
{
    static RandomGenerator *sharedRandom;
    
    @synchronized(self)
    {
        if (!sharedRandom){
            sharedRandom = [[RandomGenerator alloc] init];
            usedAnimalNames = [NSMutableSet setWithCapacity:5]; //not limited to 5 though
            usedZoneNames = [NSMutableSet setWithCapacity:5]; //not limited to 5 though

        }
        return sharedRandom;
    }
}

//----------------------Zone stuff-------------------------
-(NSString *) zoneName{
    if(zoneNames == nil){ //If not created yet
        zoneNames = [NSMutableArray arrayWithObjects: @"Happy", @"Tingly", @"Cool", @"Bubbly",
                     @"Fun fun", @"Emerald", @"Sparkly", @"Windy", @"Dark", @"Stinky",
                     @"Cozy", @"Haunted", @"Ancient", @"Amber", @"Beautiful", @"Hult",
                     @"Ryd", @"Måla", @"Ville", @"Porth", @"Ford", @"Beryl", @"Jade",
                     @"Quartz", @"Damp", @"Greasy", @"Smile", @"Icky", @"Mellow", @"Nondescipt",
                     @"Peaceful", @"Noisy", @"Secret", @"Humble", @"Grey", @"Sneaky",
                     @"Messy", @"Neat", @"Somber", @"Sleepy", @"Tiny big", @"Slippery",
                     @"Muddy", @"Very nice",nil];
    }
    
    NSString *name = @"";
    do {
        int randomIndex = arc4random() % [zoneNames count];
        name = zoneNames[randomIndex];
    }while ([usedZoneNames containsObject:name]); //Do over if already used
    
    [usedZoneNames addObject:name];
    return name;
}

-(NSString *) zoneType{
    if(zoneTypes == nil){ //If not created yet
        zoneTypes = [NSMutableArray arrayWithObjects: @"cave", @"fields", @"forest", @"lakeside", nil];
    }
    
    NSString *type = @"";
    int randomIndex = arc4random() % [zoneTypes count];
    type = zoneTypes[randomIndex];
    return type;
}

//------------------Animal stuff--------------------------

-(NSString *)species{
    if(species == nil){//not created yet
        species = [NSMutableArray arrayWithObjects: @"monkey", @"mouse", @"pig", @"cat", @"rabbit", nil];
    }
    
    NSString *newSpecies = @"";
    int randomIndex = arc4random() % [species count];
    newSpecies = species[randomIndex];
    return newSpecies;
}

-(NSString *)name{
    if(animalNames == nil){//not created yet
        animalNames = [NSMutableArray arrayWithObjects:@"Sylvester", @"Vincent", @"Digby", @"Chelsea",
                       @"Sarah", @"Ava", @"Bartolomew", @"Russel", @"Rasmus", @"Maggie", @"Muffin",
                       @"Lettie", @"Pompom", @"Sandy", @"Mario", @"Rasha", @"Zayd", @"Meera",
                       @"Edmundo", @"Piper", @"Cassandra", @"Siegmund", @"Lupe", @"Teo", @"Ingrid",
                       @"Ace", @"Muhammet", @"Rafael", @"Merrit", @"Rafferty", @"Storm", @"Huan",
                       @"Mei", @"Cora", @"Soo-jin", @"Fang", @"Bear", @"Hana", @"Tama", @"Kai",
                       @"Chibi", @"Tiny", @"Bongo", @"Morsel", @"Snickers", @"Hades", @"Pokey",
                       @"Saturn", @"Brutus", @"Boots", @"Pop", @"Berry", @"Diesel", @"Nova", @"Rocko",
                       @"Chips", @"Freckles", @"Spot", @"Dasher", @"Sinatra", @"Garland", @"Tonka",
                       @"Puffy", @"Memphis", @"Grim", @"Peggy", @"Cotton", @"Pecan", @"Captain",
                       @"Boo", @"Nibbles", @"Skippy", @"Rascal", @"Magda", @"Scar", @"Rover",
                       @"Tucket", @"Josette", @"Choochoo", @"Dallas", @"Domino", @"Fritz",
                       @"Greystoke", @"Laguna", @"Redford", @"Sheba", @"Skyler", @"Timber",
                       @"Vegas", @"Sumo", @"Leeland", nil];
    }
    
    NSString *newName = @"";
    do{
        int randomIndex = arc4random() % [animalNames count];
        newName = animalNames[randomIndex];
    }while ([usedAnimalNames containsObject: newName]); //Do over if already used
    
    [usedAnimalNames addObject:newName];
    NSLog(@"USED : %@", usedAnimalNames);
    return  newName;
}

//Gets a random phrase for meeting for the first time
-(NSString *) firstMeetingPhrase {
    if(firstMeetingPhrases == nil){//not created yet
        firstMeetingPhrases = [NSMutableArray arrayWithObjects:
                               @"Whoa! What are you??",
                               @"Have you ever met a creature as majestic as me?",
                               @"I never saw a human up close before.",
                               @"Excuse me, do you have the time??",
                               @"Do you eat animals like me?",
                               @"EEK! You startled me!",
                               @"Hmmm, you smell good!",
                               @"Hello! I've never seen a creature like you.",
                               @"You're in my territory!",
                               @"Hi! Nice to meet you, human.",
                               @"What do you call yourself?",
                               @"I always wanted to talk to a human.",
                               @"I don't trust humans, but you seem ok.",
                               @"Sorry to bother you, but do you have any food?",
                               @"Do you come here often?",
                               @"Sorry, do you have the time?",
                               @"Do you have any pets? Because you smell a little like cat.",
                               @"You look interesting!",
                               @"Allow me to introduce myself.",
                               @"Hallo! Sprechen Sie Deutsch? I'm trying to learn..",
                               @"I think your shoelace has come untied.",
                               @"Hey! Don't walk that way, it's dangerous..",
                               @"You're the third human to come here today!",
                               @"Hey, where are you heading?",
                               @"Children of the night...what music they make.",
                               @"Aren't you scared of me?",
                               @"I get really self-concious when you stare at me like that.",
                               @"DUDE! You just stepped in my meal!",
                               @"Sorry, have you seen my dad? He looks exactly like me, only bigger.",
                               @"Hey, human, wanna hear a joke?",
                               @"Hey buddy...mind helping me get out of this here trap?",
                               @"Oh! A monkey! Oh, wait..you're human.",
                               @"Do you know where the nearest bus stop is...?",
                               @"Can you spare some change?",
                               @"Can I be your pet?",
                               @"Pardon, can you help me? My back is really itchy..",
                               @"OI! Don't litter! Oh, you weren't? Never mind then..",
                               @"Say, would you like to join us for some board games? We're one person short.",
                               @"Oh, sorry for bumping into you. I was lost in thought.",
                               @"I had a dream like this last night..",
                               @"Would you like to take a short survey about trees?",
                               @"Careful! Don't step on the ants!",
                               @"Would you like to take a picture of me? Bet you don't see animals like me every day.",
                               @"I followed your tracks here. You have very strange paws!",
                               @"Such beautiful colours your fur has!",
                               @"Hello! How nice with a bit of company!",
                               @"Are you the new neighbor?",
                               @"Phew! You smell like chemicals!",
                               @"Can I tell you a story?",
                               @"Hihihi, you look strange!",
                               @"Why helloooo there, human person!",
                               @"I've never seen an ape-creature as cool as you!",
                               @"Merry Christmas! No..? Sorry, I don't really understand all of these human greetings.",
                               @"Helo. Is that right? My english is a bit rusty.",
                               @"Neek neek, fzzzzzt! Oh, sorry, you don't understand me?",
                               @"Can I walk with you a bit? I'm scared of this place..",
                               @"I like to be petted by people.",
                               @"Hi! Who are you cosplaying?",
                               @"Oh! Are you also invited to the party?",
                               @"Bask in my glory, human!",
                               nil];

    }
    
    int randomIndex = arc4random() % [firstMeetingPhrases count];
    NSString *newPhrase = firstMeetingPhrases[randomIndex];
    return newPhrase;
}

//Gets a random phrase for when you've met before
-(NSString *) normalMeetingPhrase {
    if(normalMeetingPhrases == nil){//not created yet
        normalMeetingPhrases = [NSMutableArray arrayWithObjects:
                                @"Did you bring any food?",
                                @"I was hoping you'd come again",
                                @"Can I ride on your back?",
                                @"What is your home like?",
                                @"Ooop! I just farted..",
                                @"How did you find me?",
                                @"Tell me, what's it like in the city?",
                                @"Are humans hatched from eggs?",
                                @"You can ask me anything. I might just not answer",
                                @"Listen to the wind.. Smell the trees.. Time doesn't matter here. Maybe it's all just one long dream..",
                                @"You dropped a penny! Finders keepers, hehe!",
                                @"Ehhhmmmm.. I don't know what to say.",
                                @"Are you a sir or a miss? Or something else?",
                                @"A little bird whispered in my ear that you would be here..",
                                @"Nice to see you again!",
                                @"I missed you a little.",
                                @"I didn't think you would come back!",
                                @"So..tell me more about this \"internet\" we were talking about last time!",
                                @"Tell me what it's like to be human! I wanna know everything!",
                                @"Did you have a good day?",
                                @"Do you mind lifting me up to that branch up there?",
                                @"Can I have a ride in your backpack?",
                                @"Oh, the sky is such a pretty color..",
                                @"BOOH! Haha, got you!",
                                @"Have you heard the fable about the fox and the crane?",
                                @"Should we play hide and seek?",
                                @"I have a splinter that's bothering me..could you please help me remove it?",
                                @"Yaaaawn.. I'm really sleepy, but let's hang out anyway.",
                                @"Can you swim at all with those legs?",
                                @"Don't worry, I wasn't growling at you. It was just my stomach.",
                                @"Ooops! You heard me fart? Embarrassing...",
                                @"HISSS! Whew, don't make sudden movements like that please..",
                                @"Don't come too close.. Humans make me a little nervous.",
                                @"Help!! I'm being chased by bees!",
                                nil];
    }
    
    int randomIndex = arc4random() % [normalMeetingPhrases count];
    NSString *newPhrase = normalMeetingPhrases[randomIndex];
    return newPhrase;
}

//Gets a random phrase for when you're friends
-(NSString *) friendMeetingPhrase {
    if(friendMeetingPhrases == nil){//not created yet
        friendMeetingPhrases = [NSMutableArray arrayWithObjects:
                                @"Can I visit you in your home sometime?",
                                @"Pet me please!",
                                @"You're the nicest human I ever met!",
                                @"Can I ask you something? Do you ever feel lonely..? I do sometimes, but maybe it's just because I miss my parents.",
                                @"Can I try using your smartphone? Maybe you can show me how to use it",
                                @"Hey, do you want to share this food I found?",
                                @"Do you want to go on a trip together? We go by foot, of course.. I don't think they'll let me ride on the train.",
                                @"Do you know the band called R.E.M? They're my favorite!",
                                @"You know, you should meet my friends. Maybe we'll have a party here sometime",
                                @"I can't believe I used to be afraid of you just because you're a human.",
                                @"That story you told me last time gave me nightmares!",
                                @"Do you want to have a mud bath with me?",
                                @"Small things move fast. Now you see me, now you don't.",
                                @"Let's race to the top of that hill!",
                                @"Sigh.. I really need to talk to someone. Do you mind listening....?",
                                @"You look sad, is everything alright?",
                                @"My tummy feels strange.. I shouldn't have eaten those mushrooms..",
                                nil];
    }
    
    int randomIndex = arc4random() % [friendMeetingPhrases count];
    NSString *newPhrase = friendMeetingPhrases[randomIndex];
    return newPhrase;
}


@end
