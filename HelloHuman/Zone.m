//
//  Zone.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "Zone.h"
#import "RandomGenerator.h"
#import "Animal.h"


@implementation Zone
static int levelToFamiliar = 3; //When we can search out animals
static int numberOfAnimals = 4;
static float size = 0.02; //The size of a zone

RandomGenerator *randomizer;


//Returns the size of a zone
-(float) zoneSize {
    return size;
}


-(float) halfSize {
    float n = size/2;
    return n;
}

//Returns the longitude of the eastern edge (For northeast and southeast corners)
-(float) EastLon {
    return _originLon + size;
}

//Returns the latitude of the southern edge (For southwest and southeast corners)
-(float) SouthLat {
    return _originLat + size;
}




- (instancetype)initWithLongitude:(float)longitude andLatitude:(float)latitude isHome:(Boolean) isHome{
    if (self = [super init]) {
        _originLon = longitude;
        _originLat = latitude;
        _animals = [[NSMutableArray alloc]init];
        randomizer = [RandomGenerator sharedRandom];
        _level = 0;
        
        if (!isHome){
            _namePrefix = [randomizer zoneName];
            _type = [randomizer zoneType];
        
            //Populate the array with x animals
            for (int i = 0 ; i < numberOfAnimals; i++){
                Animal *newAnimal = [[Animal alloc]initWithZone: self];
                [_animals addObject:newAnimal];
            }
        } else {
            [self makeHomeZone];
        }
    }
    return self;
}


//Returns the distance from 0,0 (simplified, longitude + latitude)
//I plan on sorting by this value later
-(int) distance{
    return (int)_originLat + _originLon;
}

-(NSString*) getFullName{
    return [NSString stringWithFormat:@"%@ %@", _namePrefix, _type ];
}

-(BOOL) isFamiliar {
    return _level >= levelToFamiliar;
}


-(NSString*) getPicUrl{
    return [NSString stringWithFormat:@"%@.jpg",_type];
}

-(void)makeHomeZone{
    _namePrefix = @"your ";
    _type = @"home turf";
}

-(Animal *) getRandomAnimal {
    int randomIndex = arc4random() % numberOfAnimals;
    Animal *randomAnimal = _animals[randomIndex];
    return randomAnimal;
}

//Gets the names of all the animals in the zone
-(NSMutableArray *) getAnimalNames {
    NSMutableArray *names = [[NSMutableArray alloc]init];
    for(int i = 0; i<_animals.count ; i++){
        Animal *thisAnimal = _animals[i];
        NSString *thisName = thisAnimal.name;
        [names addObject: thisName];
    }
    return names;
}


@end
