//
//  Animal.h
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Zone; //Forward declaration


@interface Animal : NSObject

@property NSString *name;
@property NSString *species;
@property int friendLevel;
@property NSString *picUrl;
@property Zone *homeZone;

- (instancetype)initWithZone: (Zone *) home;

-(void) increaseLevel;

-(int) levelToFriend;

@end
