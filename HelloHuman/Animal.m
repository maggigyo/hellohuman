//
//  Animal.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "Animal.h"
#import "RandomGenerator.h"
#import "Zone.h"


@implementation Animal
static int levelToFriend = 5;
RandomGenerator *randomHelper;


- (instancetype)initWithZone: (Zone *) home{
    if (self = [super init]) {
        randomHelper = [RandomGenerator sharedRandom];

    }
    _name = [randomHelper name];
    _species = [randomHelper species];
    _picUrl = [NSString stringWithFormat:@"%@.png", _species];
    _friendLevel = 0;
    _homeZone = home;
    return self;
}

-(void) increaseLevel {
    _friendLevel++;
}

-(int) levelToFriend {
    return levelToFriend;
}
@end
