//
//  Player.h
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Animal;//forward declaration

@interface Player : NSObject

@property float homeLongitude;
@property float homeLatitude;
@property NSMutableArray *friends;
@property NSMutableArray *zones;
@property int activeZoneIndex;
@property Animal *talkingToAnimal;

+ (Player *)sharedPlayer;

//Adds a zone to the array return the index of the added,
//or -1 if it failed.
- (int) addZoneWithLat:(float)originLat originLon:(float)originLon;

//Checks if we are in a zone, in that case returns the index of that zone
//Otherwise, returns -1
- (int) checkZoneWithLat:(float)latitude longitude:(float)longitude;

//For testing
-(void) printZones;

//Gets the current zone and requests a random animal from it
//Sets talkingToAnimal to this one
-(void)getRandomAnimal;

//Get the animal with this index from the current zone
//Sets talkingToAnimal to this one
-(void)getAnimalWithIndex: (int) index;

//Gets the name of the animal player is talking to
-(NSString *) getNameOfTalking;

//Gets an url to the image of the animal
-(NSString *) getPicOfTalking;

//Get a random phrase for meeting the animal depending on what friendship level it has
-(NSString *) getMeetingPhrase;

//Increases the talkingToAnimal's friendship level
-(void) increaseAnimalLevel;



@end
