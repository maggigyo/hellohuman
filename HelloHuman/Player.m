//
//  Player.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "Player.h"
#import "RandomGenerator.h"
#import "Zone.h"
#import "Animal.h"

@implementation Player

Zone* newZone;
RandomGenerator* randHelp;

+ (Player *)sharedPlayer
{
    static Player *sharedPlayer;
    
    @synchronized(self)
    {
        if (!sharedPlayer)
            sharedPlayer = [[Player alloc] init];
        
        return sharedPlayer;
    }
}

- (instancetype)init{
    if (self = [super init]) {
        _friends = [[NSMutableArray alloc]init];
        _zones = [[NSMutableArray alloc]init];
        randHelp = [RandomGenerator sharedRandom];
    }
    return self;
}

//---------------------Zone stuff---------------------------

//Adds a zone to the array return the index of the added,
//or -1 if it failed.
- (int) addZoneWithLat: (float)originLat originLon:(float)originLon {
    //Check if it fits
    
    Boolean home = NO;
    if(_zones.count < 1 ){//This is our home zone
        _activeZoneIndex = 0;
        home = YES;
    }
    newZone = [[Zone alloc]initWithLongitude:originLon andLatitude:originLat isHome: home];
    
    //Don't use the parametres from here! Use the newZones properties!
    
    
    if(_zones.count >= 1){ //We have a home, we should try to make a new zone
        
        int done = 0; //If this reaches 3, we have cleared all corners!
        
        //check if the furthest corner (southeast) would be in another zone
        done += [self assertCorner: [newZone SouthLat] longitude:[newZone EastLon]];
        //same check for the northeast corner
        done += [self assertCorner: newZone.originLat longitude:[newZone EastLon]];
        //same check for the southwest corner
        done += [self assertCorner: [newZone SouthLat] longitude: newZone.originLon];
        if(done == 3){ //All corners asserted
            
            NSLog(@"Zone origin -  lat: %f   lon: %f", newZone.originLat, newZone.originLon);
            //NSLog(@"Zone NE -  lat: %f   lon: %f", newZone.originLat, [newZone EastLon]);
            //NSLog(@"Zone SE -  lat: %f   lon: %f", [newZone SouthLat], [newZone EastLon]);
            //NSLog(@"Zone SW -  lat: %f   lon: %f", [newZone SouthLat], newZone.originLon);


            _activeZoneIndex = [_zones count]; //Last index = size before added
        } else {
            return -1; //It has failed
        }

    }
    
    [_zones addObject:newZone];
    return _activeZoneIndex;
    
}

//Will check if the corner is within a zone.
//If that's not the case it will move the new zones origin
//And try again
//If it then fails, it will return -1
- (int) assertCorner: (float)latitude longitude:(float)longitude {
    Boolean movedLat = false;//To check if the coordinates have been moved!
    Boolean movedLon = false;
    int success = [self checkZoneWithLat:(latitude)
                               longitude:(longitude)];
   
    if(success < 0){ //we have succeeded
        return 1;
    } else {
        //Try to move origin, to make room
        if(newZone.originLat != latitude){ //not in line with origin
            newZone.originLat -= [newZone halfSize]; //So we can try to move it
            movedLat = true;
            
        }
        
        if(newZone.originLon != longitude){//not in line with origin
            newZone.originLon -= [newZone halfSize]; //So we can try to move it
            movedLon = true;
        }
       
        //check if the new origin fits-------------------
        success = [self checkZoneWithLat:(newZone.originLat)
                                   longitude:(newZone.originLon)];
        
        
        if(success < 0){//It fit
            //Move coordinates if necessary-----------------------
            if(movedLat){
                latitude-= [newZone halfSize];
            }
            
            if(movedLon){
                longitude-= [newZone halfSize];
            }
            //Check if there's room now
            success = [self checkZoneWithLat:(latitude)
                                       longitude:(longitude)];
        } else {
            return -1; //Creation has failed.
        }
        
        
        if(success < 0){
            return 1; //success!
        } else {
            return -1; //creation has failed
        }
       
    }
    
}

//Checks if we are in a zone, in that case returns the index of that zone
//Otherwise, returns -1
- (int) checkZoneWithLat:(float)latitude longitude:(float)longitude {
    int zoneIndex = -1;
    for (int i = 0; i < [_zones count]; i++) {
        Zone *currentZone = _zones[i];
        
        //If both the longitude and latitude is within the zones limit
        if(latitude >= currentZone.originLat && latitude <= [currentZone SouthLat]){
            if(longitude >= currentZone.originLon && longitude <= [currentZone EastLon]){
                //We found where we are!
                zoneIndex = i;
                return zoneIndex;
            }
        }
    }
    //We didn't find anything!
    return zoneIndex;
}


//For testing purposes
-(void) printZones {
    for (int i = 0; i < [_zones count]; i++) {
        Zone *currentZone = _zones[i];
        NSLog(@" ROUND %d", i);
        NSLog(@"-- %@ --", [currentZone getFullName]);
        NSLog(@"Lat: %f  Lon: %f", currentZone.originLat, currentZone.originLon );
        NSLog(@"RESIDENTS:");
        NSMutableArray *residents = currentZone.animals;
        for (int i = 0; i < [residents count]; i++) {
            Animal *currentResident = residents[i];
            NSLog(@"%@ is a %@", currentResident.name, currentResident.species);
        }

    }

}

//----------------Animal stuff------------------------

//Gets the current zone and requests a random animal from it
//Sets talkingToAnimal to this one
-(void)getRandomAnimal {
    Zone *currentZone = _zones[_activeZoneIndex];
    _talkingToAnimal= [currentZone getRandomAnimal];

}

//Get the animal with this index from the current zone
//Sets talkingToAnimal to this one
-(void)getAnimalWithIndex: (int) index {
    Zone *currentZone = _zones[_activeZoneIndex];
    _talkingToAnimal= currentZone.animals[index];
    
}

//Gets the name of the animal player is talking to
-(NSString *) getNameOfTalking {
    return _talkingToAnimal.name;
}

-(NSString *) getPicOfTalking {
    return _talkingToAnimal.picUrl;
}

//Get a random phrase for meeting the animal depending on what friendship level it has
-(NSString *) getMeetingPhrase {
    if (_talkingToAnimal.friendLevel == 0){//You've just met
        return [randHelp firstMeetingPhrase];
        
    } else if (_talkingToAnimal.friendLevel >=
               [_talkingToAnimal levelToFriend]){ //You are friends!
        return [randHelp friendMeetingPhrase];//Get friendship phrase
        
    } else { //You've met, but you're not friends yet
        return [randHelp normalMeetingPhrase];//Get normal phrase
    }
}

//Increases the talkingToAnimal's friendship level
-(void) increaseAnimalLevel{
    [_talkingToAnimal increaseLevel];
}


@end
