//
//  Zone.h
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Animal;//forward declaration

@interface Zone : NSObject

@property int level;
@property NSString *picUrl;
@property float originLat;
@property float originLon;
@property NSString *namePrefix;
@property NSString *type;

@property NSMutableArray *animals;



-(instancetype)initWithLongitude:(float)longitude andLatitude:(float)latitude isHome:(Boolean)isHome;

-(float) zoneSize;

-(float) halfSize;

//Returns the distance from 0,0 (simplified, longitude + latitude)
-(int) distance;

//Returns the longitude of the eastern edge (For northeast and southeast corners)
-(float) EastLon;

//Returns the latitude of the southern edge (For southwest and southeast corners)
-(float) SouthLat;

-(NSString*) getFullName;

-(BOOL) isFamiliar;

//Sets the name to "Your home turf"
-(void)makeHomeZone;

//Gets the url to the picture used as background
-(NSString*) getPicUrl;

//Gets a random animal from the zone
-(Animal *) getRandomAnimal;

//Gets the names of all the animals in the zone
-(NSArray *) getAnimalNames;



@end
