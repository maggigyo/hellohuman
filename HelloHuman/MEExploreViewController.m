//
//  MEExploreViewController.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MEExploreViewController.h"
#import "Player.h"
#import "Zone.h"
@import CoreText;

@interface MEExploreViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *Image_Animal;

@property (weak, nonatomic) IBOutlet UILabel *textBox;

- (IBAction)dismissText:(UITapGestureRecognizer *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *Image_Background;

@property (weak, nonatomic) IBOutlet UIButton *Button_Home;

@property (weak, nonatomic) IBOutlet UIButton *Button_Explore;

- (IBAction)explore:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *Button_Search;

- (IBAction)search:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIPickerView *Picker;



@property (weak, nonatomic) Zone *thisZone;




@end

@implementation MEExploreViewController

NSMutableArray *_pickerData;

Player *thePlayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    thePlayer = [Player sharedPlayer];
    int index = [thePlayer activeZoneIndex];
    _thisZone = thePlayer.zones[index];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIImage *image = [UIImage imageNamed: [_thisZone getPicUrl]];
    [_Image_Background setImage:image];
    
    if( [_thisZone isFamiliar]){ //If we have leveled up the zone
        _Button_Search.hidden = NO; //We can use search!
    }
    _pickerData = [_thisZone getAnimalNames];
    self.Picker.dataSource = self;
    self.Picker.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)explore:(UIButton *)sender {
    _thisZone.level++; //Increase its level
    
    //get a new random animal to talk to
    [thePlayer getRandomAnimal];
    [self showDialogue];
}

-(void) showDialogue {
    _Button_Explore.hidden = YES;
    _Button_Home.hidden = YES;
    _Button_Search.hidden = YES;
    _textBox.hidden = NO;
    _Image_Animal.hidden = NO;
    
    //Set up the text of the message box
    NSString* message = [NSString stringWithFormat:@"%@ : %@",
                         [thePlayer getNameOfTalking],
                         [thePlayer getMeetingPhrase]];
    _textBox.text = message;
    
    //set up the image of the animal
    UIImage *image = [UIImage imageNamed: [thePlayer getPicOfTalking]];
    [_Image_Animal setImage:image];
    
    //increase the animal's friendship level
    [thePlayer increaseAnimalLevel];
    
}
- (IBAction)dismissText:(UITapGestureRecognizer *)sender {
    _Button_Explore.hidden = NO;
    _Button_Home.hidden = NO;
    _textBox.hidden = YES;
    _Image_Animal.hidden = YES;
    if( [_thisZone isFamiliar]){ //If we have leveled up the zone
        _Button_Search.hidden = NO; //We can use search!
    }
}


- (IBAction)search:(UIButton *)sender {
    int index = [_Picker selectedRowInComponent:0]; //Gets the index of selection
    
    
    //toggling the picker element
    _Picker.hidden = !(_Picker.hidden);

    if(_Picker.hidden){ //We have just hid the picker, i.e we have chosen!
        //Get the chosen animal
        [thePlayer getAnimalWithIndex:index];
        [self showDialogue]; //Start dialogue with this animal
    } else { //We are showing the picker now!
        _Button_Home.hidden = YES;
        _Button_Explore.hidden = YES;
    }
    
}

//--------Implemented UIPicker methods------------
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}


//Delegate. Sets the title (and formatting) for the rows
/* - (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = _pickerData[row];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.20 green:0.80 blue:1.00 alpha:1.0]}
    ];
 
    

    
    return attString;
    
} */

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    if (!tView){ //alloc uilabel and set properties
        tView = [[UILabel alloc] init];
        tView.textColor = [UIColor colorWithRed:0.40 green:0.90 blue:1.00 alpha:1.0];
        tView.font = [UIFont boldSystemFontOfSize:30.0];
        tView.textAlignment = NSTextAlignmentCenter;
    }
    //Set text to the current animal in array
    tView.text = _pickerData[row];
    
    return tView;
}



@end
