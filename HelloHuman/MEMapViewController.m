//
//  MEMapViewController.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MEMapViewController.h"
#import "Player.h"
#import "Zone.h"
@import CoreLocation;


@interface MEMapViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Label_ZoneName;
@property (weak, nonatomic) IBOutlet UIImageView *printsPic;
@property (weak, nonatomic) IBOutlet UIButton *exploreBtn;

- (IBAction)refresh:(UIButton *)sender;
- (IBAction)explore:(UIButton *)sender;

@end

@implementation MEMapViewController

CLLocationManager *mapLocationManager;
CLLocation *mapCurrentLocation;
Player *sPlayer;
int newZoneIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    sPlayer = [Player sharedPlayer];
    mapLocationManager = [[CLLocationManager alloc] init];
    [mapLocationManager setDelegate:self];
    mapLocationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([mapLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [mapLocationManager requestWhenInUseAuthorization];
    }
    else
    {
        nil;
    }
    [mapLocationManager startUpdatingLocation];


    
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    [mapLocationManager stopUpdatingLocation];

    int arraySize = [locations count];
    mapCurrentLocation = [locations objectAtIndex: arraySize-1]; //Get latest update
    float lat = mapCurrentLocation.coordinate.latitude;
    float lon = mapCurrentLocation.coordinate.longitude;
    //----------
    //NSLog(@"Arraysize %d", arraySize);
    //NSLog(@"Loc: %@", locations);
    //NSLog(@"my latitude :%f",lat);
    //NSLog(@"my longitude :%f",lon);
    //-------------
    
    //If it is within a zone, get index.
    newZoneIndex = [sPlayer checkZoneWithLat:lat longitude:lon];
    
    if(newZoneIndex == -1){
        //Not within zone! Create a new zone if there is room
        newZoneIndex = [sPlayer addZoneWithLat:lat originLon:lon];
    } else {
        sPlayer.activeZoneIndex = newZoneIndex; //A valid zone has been found
    }
        
    //Update the view
    [self updateViewStuff];
}

//Needs to respond to this event.
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
}

-(void) updateViewStuff{
    Zone *thisZone = sPlayer.zones[newZoneIndex];

    if(newZoneIndex == 0){
        //we are at home
        _Label_ZoneName.text = [thisZone getFullName];
        _exploreBtn.hidden = YES;
        _printsPic.hidden = YES;

    }else if (newZoneIndex <0){
        //No zone was found or created, we are in a no mans' land
        _Label_ZoneName.text = @"????????";
        _exploreBtn.hidden = YES;
        _printsPic.hidden = YES;
    }else{
        //A zone was created
        _Label_ZoneName.text = [thisZone getFullName];
        _exploreBtn.hidden = NO;
        _printsPic.hidden = NO;

    
    }
    [sPlayer printZones];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    
    [mapLocationManager stopUpdatingLocation];
}


- (IBAction)refresh:(UIButton *)sender {
    [mapLocationManager startUpdatingLocation];

}

- (IBAction)explore:(UIButton *)sender {
}
@end
