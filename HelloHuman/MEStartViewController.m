//
//  MEStartViewController.m
//  HelloHuman
//
//  Created by ITHS on 2016-05-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MEStartViewController.h"
#import "Player.h"
@import CoreLocation;
@interface MEStartViewController ()

- (IBAction)Button_GoHome:(id)sender;


@end

@implementation MEStartViewController
Player *singletonPlayer;
CLLocationManager *locationManager;
CLLocation *currentLocation;


- (void)viewDidLoad {
    [super viewDidLoad];
    singletonPlayer = [Player sharedPlayer];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Checks to see if you have set home coordinates.
//In that case performs segue.
//Otherwise, asks if you want to set them.
- (IBAction)Button_GoHome:(id)sender {
    if(singletonPlayer.zones.count > 0){ //We have a home zone
        [self performSegueWithIdentifier:@"homeSegue" sender:sender];
        NSLog(@"Home zone found");
    }
    else{
        UIAlertController* alert = [[UIAlertController alloc] init];
        alert = [UIAlertController
                                    alertControllerWithTitle:@""
                                    message:@"Are you at home right now?"
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Yes"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self setupCoordinates];
                                            }];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"No"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self notAtHome];
                                                
                                            }];
        [alert addAction:cancelAction];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }

}
//Sets up the initial Coordinates
- (void)setupCoordinates {
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    else
    {
        nil;
    }
   
    // Request a location update
    [locationManager requestLocation];

}

/* - (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    [locationManager requestLocation];
    NSLog(@"Did change auth.");

}*/


-(void)locationManager:(CLLocationManager *)manager
    didUpdateLocations:(NSArray *)locations {
    // Process the received location update
    currentLocation = [locations objectAtIndex:0];
    
    singletonPlayer.homeLatitude = currentLocation.coordinate.latitude;
    singletonPlayer.homeLongitude = currentLocation.coordinate.longitude;
    
    // Stop location updates
    [locationManager stopUpdatingLocation];
    
    [singletonPlayer addZoneWithLat: singletonPlayer.homeLatitude
                          originLon: singletonPlayer.homeLongitude];
    
    
    [self performSegueWithIdentifier:@"homeSegue" sender:self];
}

//Needs to respond to this event.
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
}


//Displays a message if the player is not at home
-(void)notAtHome {
    UIAlertController* alert2 = [UIAlertController
                                alertControllerWithTitle:@"Sorry! :("
                                message:@"You need to be at home when starting for the first time!"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) { }];
    
    [alert2 addAction:defaultAction];
    [self presentViewController:alert2 animated:YES completion:nil];
}



@end
