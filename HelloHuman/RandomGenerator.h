//
//  RandomGenerator.h
//  HelloHuman
//
//  Created by ITHS on 2016-05-08.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RandomGenerator : UITableViewController

+ (RandomGenerator *)sharedRandom;

-(NSString *) zoneName;
-(NSString *) zoneType;

-(NSString *)species;
-(NSString *)name;

//Gets a random phrase for meeting for the first time
-(NSString *) firstMeetingPhrase;

//Gets a random phrase for meeting again
-(NSString *) normalMeetingPhrase;

//Gets a random phrase for when you're friends
-(NSString *) friendMeetingPhrase;



@end
